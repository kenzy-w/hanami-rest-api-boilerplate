require "rubygems"
require 'bundler/setup'
require "hanami/setup"
require_relative '../app/application'

Hanami.configure do
  mount Web::Application, at: '/'

  environment :development do
    # See: http://hanamirb.org/guides/projects/logging
    logger level: :debug, formatter: :json

    mailer do
      delivery :test
    end
  end

  environment :production do
    logger level: :info, formatter: :json

    mailer do
      delivery :test
    end
  end
end
